﻿using CoreGraphics;
using Foundation;
using IBSTest.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using WebKit;

namespace IBSTest
{
    public partial class MasterViewController : UITableViewController
    {
        private DataSource dataSource;

        //To store opened WKWebViews
        private List<WKWebView> webViews;

        protected MasterViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = NSBundle.MainBundle.GetLocalizedString("Menu", "Menu");
            SplitViewController.PreferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible;

            TableView.Source = dataSource = new DataSource(this);

            //Prepare address bar elements depend on actual width
            PrepareAddressBar();

            //Add Image to Navigation bar
            AddNavBarImage();

            //Create test directory from json string
            CreateDirectory();

            //Add delegates to TouchUpInside for address bar buttons
            ActivateButtons();
        }

        private void PrepareAddressBar()
        {
            // Arrange button home to right edge
            var bFrame = buttonToRoot.Frame;
            var nWidth = View.Frame.Size.Width;
            var posX = nWidth - bFrame.Width;
            buttonToRoot.Frame = new CGRect(posX, bFrame.Y, bFrame.Width, bFrame.Height);

            //Correct label folder name width
            var bupFrame = buttonUpFolder.Frame;
            var lFrame = LabelRootFolder.Frame;
            LabelRootFolder.Frame = new CGRect(lFrame.X, lFrame.Y, nWidth - bFrame.Width - bupFrame.Width, lFrame.Height);
        }

        private void AddNavBarImage()
        {
            //Set logo to navigation item title view
            var image = UIImage.FromFile("Logo.png");
            var imageview = new UIImageView(image);

            var bannerWidth = NavigationController.NavigationBar.Frame.Size.Width;
            var bannerHeith = NavigationController.NavigationBar.Frame.Size.Height;

            var bannerX = bannerWidth / 2 - image.Size.Width / 2;
            var bannerY = bannerHeith / 2 - image.Size.Height / 2;
            imageview.Frame = new CoreGraphics.CGRect(bannerX, bannerY, bannerWidth, bannerHeith);
            imageview.ContentMode = UIViewContentMode.ScaleAspectFit;

            NavigationItem.TitleView = imageview;
        }

        private void CreateDirectory()
        {
            //Directore create from json string (firefox backup file)
            var t = DirectoryService.CreateTree().FirstOrDefault();

            // TypeCode = 0 means directory root
            t.TypeCode = 0;
            PopulateTable(t);
        }

        private void PopulateTable(DirectoryNode item)
        {
            dataSource.Objects.Clear();
            if (item == null) return;
            foreach (var node in item.Children)
            {
                node.Parent = item;
                dataSource.Objects.Add(node);
            }
            TableView.ReloadData();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            if (segue.Identifier == "showDetail")
            {
                var indexPath = TableView.IndexPathForSelectedRow;
                var item = dataSource.Objects[indexPath.Row];

                // Url cell type only processing
                if (item.TypeCode == 1)
                {
                    var controller = (DetailViewController)((UINavigationController)segue.DestinationViewController).TopViewController;
                    if (webViews == null) webViews = new List<WKWebView>();
                    webViews = controller.LoadWebPage(item, webViews);

                    SplitViewController.PreferredDisplayMode = UISplitViewControllerDisplayMode.PrimaryHidden;
                    controller.NavigationItem.LeftBarButtonItem = SplitViewController.DisplayModeButtonItem;
                    controller.NavigationItem.LeftItemsSupplementBackButton = true;
                }
            }
        }

        public void ProcessFolder(DirectoryNode item)
        {
            //Show navigate buttons
            buttonToRoot.Enabled = true;
            buttonUpFolder.Enabled = true;

            //Arrange address bar elements depend on actual width
            PrepareAddressBar();

            // Set directory name to address bar label
            LabelRootFolder.Text = item.Title;

            // Clear tableview list and populate with current items children
            PopulateTable(item);
        }

        private void ActivateButtons()
        {
            // Add event delegate to process Back button
            buttonUpFolder.TouchUpInside += (object senderb, EventArgs e) =>
            {
                var currentItem = dataSource.Objects.FirstOrDefault().Parent;
                LabelRootFolder.Text = currentItem.Parent.Title;

                if (currentItem.Parent.TypeCode == 0)
                {
                    AddressBarToRootState();
                }

                // Clear tableview list and populate with current item parent children
                dataSource.Objects.Clear();
                PopulateTable(currentItem.Parent);
            };

            // Recreate Directory on home button
            buttonToRoot.TouchUpInside += (object senderb, EventArgs e) =>
            {
                AddressBarToRootState();
                CreateDirectory();
            };
        }

        private void AddressBarToRootState()
        {
            LabelRootFolder.Text = "...";
            buttonToRoot.Enabled = false;
            buttonUpFolder.Enabled = false;
        }

        private class DataSource : UITableViewSource
        {
            private static readonly NSString CellIdentifier = new NSString("Cell");
            private readonly List<DirectoryNode> objects = new List<DirectoryNode>();
            private readonly MasterViewController controller;

            public DataSource(MasterViewController controller)
            {
                this.controller = controller;
            }

            public IList<DirectoryNode> Objects
            {
                get { return objects; }
            }

            // Customize the number of sections in the table view.
            public override nint NumberOfSections(UITableView tableView)
            {
                return 1;
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return objects.Count;
            }

            // Customize the appearance of table view cells.
            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                var cell = tableView.DequeueReusableCell(CellIdentifier, indexPath);

                var itemForCell = objects[indexPath.Row];
                cell.TextLabel.Text = itemForCell.Title;

                var ifButtons = cell.Subviews.Where(r => r.GetType() == typeof(UIButton));
                if (ifButtons != null)
                {
                    foreach (var item in ifButtons)
                    {
                        item.RemoveFromSuperview();
                        item.Dispose();
                    }
                }

                //Cell type = folder
                if (objects[indexPath.Row].TypeCode == 2)
                {
                    //Prepare button to process folder tap
                    UIButton button = new UIButton();
                    button.Frame = new CGRect(0, 0, cell.Frame.Width, cell.Frame.Height);
                    button.TouchUpInside += (object sender, EventArgs e) =>
                    {
                        controller.ProcessFolder(itemForCell);
                    };
                    cell.AddSubview(button);
                    cell.TextLabel.Font = UIFont.FromName("HelveticaNeue-Bold", 18);
                    cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                }

                //Cell type = url
                if (objects[indexPath.Row].TypeCode == 1)
                {
                    cell.TextLabel.Font = UIFont.FromName("HelveticaNeue", 18);
                    cell.Accessory = UITableViewCellAccessory.None;
                }

                return cell;
            }

            public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
            {
                // Return false if you do not want the specified item to be editable.
                return true;
            }

            public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
            {
                if (editingStyle == UITableViewCellEditingStyle.Delete)
                {
                    // Delete the row from the data source.
                    objects.RemoveAt(indexPath.Row);
                    controller.TableView.DeleteRows(new[] { indexPath }, UITableViewRowAnimation.Fade);
                }
                else if (editingStyle == UITableViewCellEditingStyle.Insert)
                {
                    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
                }
            }
        }
    }
}