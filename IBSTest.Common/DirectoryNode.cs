﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IBSTest.Common
{
    public class DirectoryNode
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("typeCode")]
        public int TypeCode { get; set; }

        [JsonProperty("uri")]
        public string Uri { get; set; }

        [JsonProperty("children")]
        public List<DirectoryNode> Children { get; set; }

        public DirectoryNode Parent { get; set; }
    }
}