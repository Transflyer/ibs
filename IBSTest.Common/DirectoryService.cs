﻿using System;
using System.Collections.Generic;
using IBSTest.Common.Constants;
using Newtonsoft.Json;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace IBSTest.Common
{
    public static class DirectoryService
    {
        /// <summary>
        /// Return directory tree from Firefox backup json string
        /// </summary>
        /// <param name="jsonFirefox"></param>
        /// <returns></returns>
        public static List<DirectoryNode> CreateTree()
        {
            var root = CreateTree(DirectoryConstants.FireFoxJsonString1);
            if (root == null) return null;

            return root.Children;
        }

        private static DirectoryNode CreateTree(string jsonFirefox)
        {
            try
            {
                return JsonConvert.DeserializeObject<DirectoryNode>(jsonFirefox);
            }
            catch
            {
                return null;
            }
        }



    }
}