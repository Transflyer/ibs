﻿using CoreGraphics;
using Foundation;
using IBSTest.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using UIKit;
using WebKit;

namespace IBSTest
{
    public partial class DetailViewController : UIViewController, IWKNavigationDelegate
    {
        public object DetailItem { get; set; }
        /// <summary>
        /// Collection of opened WKWebViews
        /// </summary>
        public List<WKWebView> webViews;

        /// <summary>
        /// Buttons coolection for page tabs
        /// </summary>
        private List<UIButton> buttons = new List<UIButton>();

        private UIButton activeButton = new UIButton();
        private UIView rootScrollView = new UIView();
        private UIScrollView scrollView = new UIScrollView();
        private UIView inScrollView = new UIView();

        //Tab buttons visual settings 
        private readonly int buttonWidth = 150;
        private readonly int buttonSeparateSpace = 15;
        private readonly int buttonCornerRadius = 15;
        private readonly UIColor buttonNormalStateColor = UIColor.Blue;
        private readonly UIColor buttonSelectedStateColor = UIColor.LightGray;

        public DetailViewController(IntPtr handle) : base(handle)
        {
        }

        public void SetDetailItem(object newDetailItem)
        {
            if (DetailItem != newDetailItem)
            {
                DetailItem = newDetailItem;
                // Update the view
                ConfigureView();
            }
        }

        private void ConfigureView()
        {
            //Set rigth gesture to main view if no one tab is opened
            UISwipeGestureRecognizer swipeRigth = new UISwipeGestureRecognizer(OnSwipeRight);
            swipeRigth.Direction = UISwipeGestureRecognizerDirection.Right;
            View.AddGestureRecognizer(swipeRigth);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Perform any additional setup after loading the view, typically from a nib.
            ConfigureView();
        }

        private void AddRightSwipe(WKWebView webView)
        {
            // Add help view to WKWebView to make right gesture to show menu
            UIView uIView = new UIView();
            var u = webView.Subviews.FirstOrDefault(r => r.Tag == 5890);
            if (u != null)
            {
                u.RemoveFromSuperview();
                u.Dispose();
            }

            UISwipeGestureRecognizer swipeRigth = new UISwipeGestureRecognizer(OnSwipeRight);
            swipeRigth.Direction = UISwipeGestureRecognizerDirection.Right;

            uIView.Tag = 5890;
            uIView.Frame = new CGRect(0, 0, 100, webView.Frame.Height);

            uIView.AddGestureRecognizer(swipeRigth);
            webView.AddSubview(uIView);
        }

        private void OnSwipeRight()
        {
            // Show menu from split controller
            if (SplitViewController == null) return;
            SplitViewController.PreferredDisplayMode = UISplitViewControllerDisplayMode.PrimaryOverlay;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        internal List<WKWebView> LoadWebPage(DirectoryNode item, List<WKWebView> passedwebViews)
        {
            webViews = passedwebViews;
            NSUrl url = new NSUrl(item.Uri);

            // Create WKWebView and URL proccessing
            var frame = new CGRect(View.Frame.X, View.Frame.Y, View.Frame.Width, View.Frame.Height);
            var webView = new WKWebView(frame, new WKWebViewConfiguration());
            View.AddSubview(webView);
            webView.LoadRequest(new NSUrlRequest(url));

            // Add to web pages local collection
            webViews.Add(webView);
            webView.NavigationDelegate = (IWKNavigationDelegate)this;

            //Prepere tab buttons for all existed pages
            PrepareTabButtons(webViews);

            // Set scrollview content offset to focus on active tab
            ScrollToFocusActiveTab();

            //Add right swipe gesture to webview to call menu
            AddRightSwipe(webView);

            return webViews;
        }

        private void ScrollToFocusActiveTab()
        {
            var sWidth = scrollView.ContentSize.Width;
            var vWidth = View.Frame.Width;

            if (sWidth > vWidth)

            {
                var offset = new CGPoint(sWidth - vWidth, 0);
                scrollView.SetContentOffset(offset, true);
            }
        }

        [Export("webView:didFinishNavigation:")]
        public virtual void DidFinishNavigation(WebKit.WKWebView webView, WebKit.WKNavigation navigation)
        {
            //Process finish loading - update titles of tab buttons
            if (webViews.Count != buttons.Count) return;
            List<string> titles = new List<string>();
            foreach (var item in webViews)
            {
                titles.Add(item.Title);
            }
            var titleArray = titles.ToArray();
            int i = 0;
            foreach (var item in buttons)
            {
                item.SetTitle(titleArray[i], UIControlState.Normal);
                i++;
            }
        }

        private void PrepareTabButtons(List<WKWebView> webViews)
        {
            if (webViews == null) return;

            //Prepare scroll view to keep opened tabs in navigaton bar
            PrepareScrollView();

            // Prepare navigation buttons
            foreach (var item in webViews)
            {
                // Prepare navigation button
                UIButton button = CreateButtonNavigation(inScrollView, item.Title);

                // Prepare button close in navigation tab button
                UIButton buttonClose = CreateButtonClose(button, item);

                button.TouchUpInside += (object sender, EventArgs e) =>
                {
                    // Set previous pushed button to unselected condition
                    UnSelectButton();
                    button.AddSubview(buttonClose);
                    button.BackgroundColor = UIColor.LightGray;
                    AddRightSwipe(item);
                    View.AddSubview(item);
                };

                //Set active tab in navigation panel
                SetActiveTab(button, item, buttonClose);

                //Add button to local collection
                buttons.Add(button);
            }

            // Update scroll view and its subviews
            UpdateScrollView();
        }

        private void PrepareScrollView()
        {
            var navigationFrame = this.NavigationController.NavigationBar.Frame;

            // Prepare root navigation view for scrolling page bookmarks
            rootScrollView = new UIView();
            rootScrollView.Frame = new CGRect(100, 0, View.Frame.Width, navigationFrame.Height);
            NavigationController.NavigationBar.AddSubview(rootScrollView);

            // Prepare scroll view
            scrollView = new UIScrollView();
            scrollView.Frame = new CGRect(0, 0, rootScrollView.Frame.Width, rootScrollView.Frame.Height);
            rootScrollView.AddSubview(scrollView);

            // Prepare view in scroll view
            inScrollView = new UIView();
            inScrollView.Frame = new CGRect(0, 0, rootScrollView.Frame.Width, rootScrollView.Frame.Height);
            scrollView.AddSubview(inScrollView);
        }

        private void SetActiveTab(UIButton button, WKWebView item, UIButton buttonClose)
        {
            var activeView = View.Subviews.FirstOrDefault(r => r == item);
            if (activeView != null)
            {
                UnSelectButton();
                button.AddSubview(buttonClose);
                button.BackgroundColor = UIColor.LightGray;
            }
        }

        private UIButton CreateButtonNavigation(UIView inScrollView, string buttonTitle)
        {
            //Create Tab Button
            UIButton button = new UIButton();
            var posX = buttons.Count * buttonWidth + buttons.Count * buttonSeparateSpace;
            button.Layer.CornerRadius = 10;
            button.Layer.BorderWidth = 1;

            button.Frame = new CGRect(posX, 5, buttonWidth, inScrollView.Frame.Height - 10);
            button.SetTitle(buttonTitle, UIControlState.Normal);
            button.BackgroundColor = buttonNormalStateColor;

            return button;
        }

        private UIButton CreateButtonClose(UIButton button, WKWebView item)
        {
            //Create button to close active tab button
            UIButton buttonClose = new UIButton();
            buttonClose.Frame = new CGRect(5, 5, 30, 30);
            var image = UIImage.FromFile("close.png");
            buttonClose.SetImage(image, UIControlState.Normal);
            buttonClose.Layer.CornerRadius = buttonCornerRadius;
            buttonClose.Layer.BorderWidth = 1;
            buttonClose.BackgroundColor = UIColor.White;

            buttonClose.TouchUpInside += (object sender, EventArgs e) =>
            {
                // Remove this button from buttons collection
                int index = buttons.FindIndex(a => a == button);
                buttons.Remove(button);
                button.RemoveFromSuperview();

                //Move other buttons to deleted button place
                var buttonsToUpdate = buttons.Skip(index).ToList();
                foreach (var bItem in buttonsToUpdate)
                {
                    var frame = bItem.Frame;
                    bItem.Frame = new CGRect(frame.X - buttonWidth - buttonSeparateSpace, frame.Y, frame.Width, frame.Height);
                }

                // Remove page from WKWebViews collection
                var indexView = webViews.FindIndex(a => a == item);
                webViews.Remove(item);
                item.RemoveFromSuperview();

                //Choose active page after deleting one
                WKWebView activeView = null;
                if (indexView == webViews.Count && webViews.Count != 0)
                {
                    // When deleted last tab set active tab from left
                    activeView = webViews.Last();
                    View.AddSubview(activeView);
                }
                else
                {
                    // When had deleted element set active tab from right
                    if (webViews.Count >= indexView && webViews.Count != 0)
                    {
                        activeView = webViews[indexView];
                        View.AddSubview(activeView);
                    }
                }

                if (activeView != null)
                {
                    var i = webViews.FindIndex(a => a == activeView);
                    var activeTabButton = buttons[i];
                    var activeCloseButton = CreateButtonClose(activeTabButton, activeView);
                    SetActiveTab(activeTabButton, activeView, activeCloseButton);
                }
                // Update scroll view and its subviews
                UpdateScrollView();
            };
            return buttonClose;
        }

        private void UnSelectButton()
        {
            var prevSelectedbutton = buttons.FirstOrDefault(r => r.BackgroundColor == UIColor.LightGray);
            if (prevSelectedbutton != null)
            {
                var bClose = prevSelectedbutton.Subviews.FirstOrDefault(r => r.GetType() == typeof(UIButton));
                prevSelectedbutton.BackgroundColor = buttonNormalStateColor;
                var tlFrame = prevSelectedbutton.TitleLabel.Frame;
                prevSelectedbutton.TitleRectForContentRect(new CGRect(tlFrame.X - 40, tlFrame.Y, tlFrame.Width, tlFrame.Height));
                if (bClose != null) bClose.RemoveFromSuperview();
            }
        }

        private void UpdateScrollView()
        {
            // Set inScrollView content width
            var fr = inScrollView.Frame;
            var width = buttons.Count * buttonWidth + buttons.Count * buttonSeparateSpace + 100;
            inScrollView.Frame = new CGRect(fr.X, fr.Y, width, fr.Height);

            // Set scrollView ContentSize
            scrollView.ContentSize = inScrollView.Frame.Size;

            // Add buttons to inscroll view
            inScrollView.AddSubviews(buttons.ToArray());

            // Add rootScrollview to navigation bar
            NavigationController.NavigationBar.AddSubview(rootScrollView);
        }
    }
}