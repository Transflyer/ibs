﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace IBSTest
{
    [Register ("MasterViewController")]
    partial class MasterViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton buttonToRoot { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton buttonUpFolder { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel LabelRootFolder { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView viewAddressBar { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (buttonToRoot != null) {
                buttonToRoot.Dispose ();
                buttonToRoot = null;
            }

            if (buttonUpFolder != null) {
                buttonUpFolder.Dispose ();
                buttonUpFolder = null;
            }

            if (LabelRootFolder != null) {
                LabelRootFolder.Dispose ();
                LabelRootFolder = null;
            }

            if (viewAddressBar != null) {
                viewAddressBar.Dispose ();
                viewAddressBar = null;
            }
        }
    }
}